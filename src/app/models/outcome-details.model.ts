export class OutcomeDetails {
    betId: number;
    datePlaced: string;
    betLineId: number;
    outcomeId: number;
    status: string;

    constructor(betId: number, datePlaced: string, betLineId: number, outcomeId: number, status: string) {
        this.betId = betId;
        this.datePlaced = new Date(datePlaced).toLocaleDateString('en-GB', {
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric'
        });
        this.betLineId = betLineId;
        this.outcomeId = outcomeId;
        this.status = status;
    }
}

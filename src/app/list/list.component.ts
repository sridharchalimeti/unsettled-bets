import { Component, OnInit } from '@angular/core';

const content = [
  {
    "id": 2069,
    "datePlaced": "2019-05-10T15:33:46.432+0000",      
    "lines": [
      {
        "id": 6158,
        "outcomeId": 8806309593370624,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6159,          
        "outcomeId": 8806309595625600,
        "status": {
          "id": -10,
          "description": "Running"
        }
      }
    ]
  },
  {
    "id": 2070,
    "datePlaced": "2019-05-10T15:33:46.502+0000",     
    "lines": [
      {
        "id": 6160,
        "outcomeId": 8806309593370624,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6161,
        "outcomeId": 5783448930382080,
        "status": {
          "id": -10,
          "description": "Running"
        }
      }
    ]
  },
  {
    "id": 2071,
    "datePlaced": "2019-05-10T15:33:46.561+0000",
    "lines": [
      {
        "id": 6162,
        "outcomeId": 8806309593370624,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6163,
        "outcomeId": 8806309595495808,
        "status": {
          "id": -10,
          "description": "Running"
        }
      }
    ]
  },
  {
    "id": 2072,
    "datePlaced": "2019-05-10T15:33:46.620+0000",
    "lines": [
      {
        "id": 6164,
        "outcomeId": 8806309595625600,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6165,
        "outcomeId": 5783448930382080,
        "status": {
          "id": -10,
          "description": "Running"
        }
      }
    ]
  },
  {
    "id": 2073,
    "datePlaced": "2019-05-10T15:33:46.675+0000",
    "lines": [
      {
        "id": 6166,
        "outcomeId": 8806309595625600,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6167,
        "outcomeId": 8806309595495808,
        "status": {
          "id": -10,
          "description": "Running"
        }
      }
    ]
  },
  {
    "id": 2074,
    "datePlaced": "2019-05-10T15:33:46.729+0000",
    "lines": [
      {
        "id": 6168,
        "outcomeId": 5783448930382080,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6169,
        "outcomeId": 8806309595495808,
        "status": {
          "id": -10,
          "description": "Running"
        }
      }
    ]
  },
  {
    "id": 2075,
    "datePlaced": "2019-05-10T15:33:46.785+0000",
    "lines": [
      {
        "id": 6170,
        "outcomeId": 8806309593370624,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6171,
        "outcomeId": 8806309595625600,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6172,
        "outcomeId": 5783448930382080,
        "status": {
          "id": -10,
          "description": "Running"
        }
      }
    ]
  },
  {
    "id": 2076,
    "datePlaced": "2019-05-10T15:33:46.851+0000",
    "lines": [
      {
        "id": 6173,
        "outcomeId": 8806309593370624,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6174,
        "outcomeId": 8806309595625600,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6175,
        "outcomeId": 8806309595495808,
        "status": {
          "id": -10,
          "description": "Running"
        }
      }
    ]
  },
  {
    "id": 2077,
    "datePlaced": "2019-05-10T15:33:46.917+0000",
    "lines": [
      {
        "id": 6176,
        "outcomeId": 8806309593370624,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6177,
        "outcomeId": 5783448930382080,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6178,
        "outcomeId": 8806309595495808,
        "status": {
          "id": -10,
          "description": "Running"
        }
      }
    ]
  },
  {
    "id": 2078,
    "datePlaced": "2019-05-10T15:33:46.984+0000",
    "lines": [
      {
        "id": 6179,
        "outcomeId": 8806309595625600,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6180,
        "outcomeId": 5783448930382080,
        "status": {
          "id": -10,
          "description": "Running"
        }
      },
      {
        "id": 6181,
        "outcomeId": 8806309595495808,
        "status": {
          "id": -10,
          "description": "Running"
        }
      }
    ]
  }
];

import { OutcomeDetails } from '../models/outcome-details.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  data: OutcomeDetails[] = [];

  constructor() {
    //console.log(content);
    content.forEach(bet => {
      bet.lines.forEach(betLine => {
        const dataElement = new OutcomeDetails(bet.id, bet.datePlaced, betLine.id, betLine.outcomeId, betLine.status.description);
        this.data.push(dataElement);
        /// console.log(dataElement);
      });
    });
    //console.log(this.data);
  }

  ngOnInit() {
  }

}

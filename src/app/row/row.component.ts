import { Component, OnInit } from '@angular/core';
import { FieldComponent } from '../field/field.component';

@Component({
  selector: 'app-row',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.css']
})
export class RowComponent implements OnInit {

  column: FieldComponent;

  constructor() { }

  ngOnInit() {
  }

}
